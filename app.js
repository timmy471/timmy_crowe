var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var port = process.env.PORT || 8080;
var IP = process.env.IP;



app.set('view engine', 'ejs');
 var urlencodedParser = bodyParser.urlencoded({extended:false});
app.use(express.static('public'));
app.use(express.static(__dirname + '/public'));



app.get('/', function(req, res){
    res.render('index');
})

app.get('/contact', function(req, res){
    res.render('contact');
})

app.post('/contact', urlencodedParser, function(req, res){
    console.log(req.body.mail);
    res.render('contact-success');
})





app.listen(port, IP, function(){
    console.log('listening on' + port);
})

